_config
=======

![](https://cloud.githubusercontent.com/assets/9628112/5490020/a5ecfb2c-86ce-11e4-89da-5a7fcee9def8.png)

This repo contains a set of config files for Vim and Oh-my-zsh, along with an installation script.

Main sources:
- https://github.com/QuentinPerez/42-toolkit
- https://github.com/gponsinet/dotfiles
