#!/bin/sh

# Saving current config ###########################################

save(){
	if [ -d "./save" ]; then 
		echo "\033[1;31mUnexpected ./save\033[0m"
		echo "\033[1;31mInstallation canceled\033[0m"
		exit
	fi
	mkdir save
	echo "\033[1;33m*** Saving current config ***\033[0m"
	if [ -e "$HOME/.zshrc" ]; then
	cp ~/.zshrc						./save/zshrc.save
	echo	"~/.zshrc"
	fi
	if [ -e "$HOME/.vimrc" ]; then
	cp ~/.vimrc						./save/vimrc.save
	echo	"~/.vimrc"
	fi
	if [ -e "$HOME/.gitconfig" ]; then
	cp ~/.gitconfig					./save/gitconfig.save
	echo	"~/.gitconfig"
	fi
	if [ -e "$HOME/.gitignore_global" ]; then
	cp ~/.gitignore_global			./save/gitignore_global.save
	echo	"~/.gitignore_global"
	fi
}

# Installation scripts ############################################

update_brew(){
	echo "\033[1;33m*** Config: Brew ***\033[0m"
	export HOMEBREW_TEMP="/tmp/brew.tmp"
	export HOMEBREW_CACHE="/tmp/brew.cache"
	rm -rf $HOMEBREW_TEMP $HOMEBREW_TEMP
	mkdir -p $HOMEBREW_TEMP $HOMEBREW_TEMP
	brew update
	brew install jsaglio42/tap/makeBuildC
	brew install jsaglio42/tap/makeBuildCpp
}

update_oh_my_zsh(){
	if [ ! -d "$HOME/.oh-my-zsh" ]; then 
		echo "\033[1;33m*** Install: OMZSH ***\033[0m"
		sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	fi
	echo "\033[1;33m*** Config: OMZSH ***\033[0m"
	cp ./srcs/oh-my-zsh/zshrc				~/.zshrc
	echo	"~/.zshrc"
	cp ./srcs/oh-my-zsh/jsaglio.zsh-theme		~/.oh-my-zsh/themes/
	echo	"~/.oh-my-zsh/themes/"
}

update_VIM(){
	echo "\033[1;33m*** Config: VIM ***\033[0m"
	cp ./srcs/vim/vimrc				~/.vimrc
	echo	"~/.vimrc"
}

update_git(){
	echo "\033[1;33m*** Config: GIT ***\033[0m"
	cp ./srcs/git/gitconfig			~/.gitconfig
	echo	"~/.gitconfig"
	cp ./srcs/git/gitignore_global			~/.gitignore_global
	echo	"~/.gitignore_global"
}

update_spectacle(){
	echo "\033[1;33m*** Config: Spectacle ***\033[0m"
	cp ./srcs/Spectacle/com.divisiblebyzero.Spectacle.plist				~/Library/Preferences
}

usage(){
	echo "usage: ./install.sh [-all] [-omzsh] [-brew] [-vim] [-git] [-spectacle]"
	exit -1
}

install(){
	echo "\033[1;33m*** Reading arguments ***\033[0m"
	for var in "$@"
	do
		if [ $var == "-all" ]; then
			update_oh_my_zsh
			update_brew
			update_VIM
			update_git
			break
		elif [ $var == "-omzsh" ]; then
			update_oh_my_zsh
		elif [ $var == "-brew" ]; then
			update_brew
		elif [ $var == "-vim" ]; then
			update_VIM
		elif [ $var == "-git" ]; then
			update_git
		elif [ $var == "-spectacle" ]; then
			update_spectacle
		else
			usage
		fi
	done
	
}

# Main #############################################################

if [ "$#" -ne 1 ]; then 
	usage
fi
if [ -e "./save" ]; then
	read -r -p "\"save\" exists. Overwrite? [y/n] " response
	case $response in
		[yY])
			rm -rf ./save
			;;
		*)
			echo "\033[1;31mInstallation canceled\033[0m"
			exit
			;;
	esac
fi
save
install $@
echo "\033[1;32mInstallation succeeded.\033[0m"
